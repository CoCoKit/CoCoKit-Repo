Pod::Spec.new do |s|
  s.name = "CoCoUIKit"
  s.version = "1.1.0.33"
  s.summary = "CoCoUIKit 用来解析字典和数组类型中的基本类型，防止错误类型造成的Crash."
  s.license = "MIT"
  s.requires_arc = true
  s.authors = {"iScarlett"=>"iScarlett@qq.com"}
  s.homepage = "https://gitlab.com/CoCoKit"
  s.description = "CoCoUIKit由自动化打包脚本生成提交,由最新push持续集成,保持最新" 
  s.source = { :git => 'https://gitlab.com/CoCoKit/CoCoUIKit-framework.git' }
  s.ios.deployment_target = '8.0'
  s.ios.vendored_framework = 'CoCoUIKit.framework'
  s.dependency 'Masonry' ,'~> 1.0.2'
  s.dependency 'pop' ,'~> 1.0.9'
  s.dependency 'CoCoCategorys' ,'>= 0'
end
