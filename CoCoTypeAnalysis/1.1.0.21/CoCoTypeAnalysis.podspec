Pod::Spec.new do |s|
  s.name = "CoCoTypeAnalysis"
  s.version = "1.1.0.21"
  s.summary = "CoCoTypeAnalysis 用来解析字典和数组类型中的基本类型，防止错误类型造成的Crash."
  s.license = "MIT"
  s.requires_arc = true
  s.authors = {"iScarlett"=>"iScarlett@qq.com"}
  s.homepage = "https://gitlab.com/CoCoKit"
  s.description = "CoCoTypeAnalysis由自动化打包脚本生成提交,由最新push持续集成,保持最新" 
  s.source = { :git => 'https://gitlab.com/CoCoKit/CoCoTypeAnalysis-framework.git' }
  s.ios.deployment_target = '8.0'
  s.ios.vendored_framework = 'CoCoTypeAnalysis.framework'
  s.framework = 'AssetsLibrary','AVFoundation','MessageUI','CoreTelephony'
end
