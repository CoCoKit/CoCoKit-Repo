Pod::Spec.new do |s|
  s.name = "CoCoImageContainer"
  s.version = "1.1.0.54"
  s.summary = "CoCoImageContainer 用来解析字典和数组类型中的基本类型，防止错误类型造成的Crash."
  s.license = "MIT"
  s.requires_arc = true
  s.authors = {"iScarlett"=>"iScarlett@qq.com"}
  s.homepage = "https://gitlab.com/CoCoKit"
  s.description = "CoCoImageContainer由自动化打包脚本生成提交,由最新push持续集成,保持最新" 
  s.source = { :git => 'https://gitlab.com/CoCoKit/CoCoImageContainer-framework.git' }
  s.ios.deployment_target = '8.0'
  s.ios.vendored_framework = 'CoCoImageContainer.framework'
  s.resource = "Resources/*.bundle"
  s.dependency 'Masonry' ,'~> 1.0.2'
  s.dependency 'CoCoReactiveCocoa' ,'>= 0'
  s.dependency 'CoCoCategorys' ,'>= 0'
  s.dependency 'CoCoUIKit' ,'>= 0'
  s.dependency 'CoCoKit' ,'>= 0'
end
