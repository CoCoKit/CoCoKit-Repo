Pod::Spec.new do |s|
  s.name = "CoCoCategorys"
  s.version = "1.1.0.49"
  s.summary = "CoCoCategorys 是CoCo框架下的工具分类"
  s.license = "MIT"
  s.requires_arc = true
  s.authors = {"iScarlett"=>"iScarlett@qq.com"}
  s.homepage = "https://gitlab.com/CoCoKit"
  s.description = "CoCoCategorys由自动化打包脚本生成提交,由最新push持续集成,保持最新" 
  s.source = { :git => 'https://gitlab.com/CoCoKit/CoCoCategorys-framework.git' }
  s.ios.deployment_target = '8.0'
  s.ios.vendored_framework = 'CoCoCategorys.framework'
  s.framework = 'AssetsLibrary','Accelerate','CoreMotion','AddressBook','CoreBluetooth','EventKit','Foundation','UIKit','Photos','AVFoundation','CoreLocation'
  s.dependency 'CoCoKit' ,'>= 0'
end
