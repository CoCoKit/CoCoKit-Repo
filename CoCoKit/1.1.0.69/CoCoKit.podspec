Pod::Spec.new do |s|
  s.name = "CoCoKit"
  s.version = "1.1.0.69"
  s.summary = "CoCoKit 是CoCo框架的基础框架"
  s.license = "MIT"
  s.requires_arc = true
  s.authors = {"iScarlett"=>"iScarlett@qq.com"}
  s.homepage = "https://gitlab.com/CoCoKit"
  s.description = "CoCoKit由自动化打包脚本生成提交,由最新push持续集成,保持最新" 
  s.source = { :git => 'https://gitlab.com/CoCoKit/CoCoKit-framework.git' }
  s.ios.deployment_target = '8.0'
  s.ios.vendored_framework = 'CoCoKit.framework'
end
