Pod::Spec.new do |s|
  s.name = "CoCoActionKit"
  s.version = "1.0.0.83"
  s.summary = "CoCoActionKit 是CoCo框架下的工具分类"
  s.license = "MIT"
  s.requires_arc = true
  s.authors = {"iScarlett"=>"iScarlett@qq.com"}
  s.homepage = "https://gitlab.com/CoCoKit"
  s.description = "CoCoActionKit由自动化打包脚本生成提交,由最新push持续集成,保持最新" 
  s.source = { :git => 'https://gitlab.com/CoCoKit/CoCoActionKit-framework.git' }
  s.ios.deployment_target = '8.0'
  s.ios.vendored_framework = 'CoCoActionKit.framework'
  s.framework = 'Foundation','UIKit'
end
