Pod::Spec.new do |s|
  s.name = "CoCoFilterKit"
  s.version = "1.0.0.22"
  s.summary = "CoCoFilterKit 用来解析字典和数组类型中的基本类型，防止错误类型造成的Crash."
  s.license = "MIT"
  s.requires_arc = true
  s.authors = {"iScarlett"=>"iScarlett@qq.com"}
  s.homepage = "https://gitlab.com/CoCoKit"
  s.description = "CoCoFilterKit由自动化打包脚本生成提交,由最新push持续集成,保持最新" 
  s.source = { :git => 'https://gitlab.com/CoCoKit/CoCoFilterKit-framework.git' }
  s.ios.deployment_target = '9.0'
  s.ios.vendored_framework = 'CoCoFilterKit.framework'
  s.resource = "Resources/*.bundle"
  s.dependency 'CoCoCategorys' ,'>= 0'
  s.dependency 'CoCoGPUImage' ,'>= 0'
  s.dependency 'CoCoKit' ,'>= 0'
  s.dependency 'CoCoTool' ,'>= 0'
end
