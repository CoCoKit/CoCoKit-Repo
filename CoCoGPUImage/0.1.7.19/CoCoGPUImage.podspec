Pod::Spec.new do |s|
  s.name = "CoCoGPUImage"
  s.version = "0.1.7.19"
  s.summary = "An open source iOS framework for GPU-based image and video processing."
  s.license = "BSD"
  s.requires_arc = true
  s.xcconfig = {"CLANG_MODULES_AUTOLINK"=>"YES"}
  s.authors = {"iScarlett"=>"iScarlett@qq.com"}
  s.homepage = "https://gitlab.com/CoCoKit"
  s.description = "CoCoGPUImage由自动化打包脚本生成提交,由最新push持续集成,保持最新" 
  s.source = { :git => 'https://gitlab.com/CoCoKit/CoCoGPUImage-framework.git' }
  s.ios.deployment_target = '8.0'
  s.ios.vendored_framework = 'CoCoGPUImage.framework'
  s.resource = "Resources/*.bundle"
  s.framework = 'OpenGLES','CoreMedia','QuartzCore','AVFoundation'
end
