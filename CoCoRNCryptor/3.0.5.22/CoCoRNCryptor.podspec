Pod::Spec.new do |s|
  s.name = "CoCoRNCryptor"
  s.version = "3.0.5.22"
  s.summary = "Encryptor/Decryptor for iOS."
  s.license = "MIT"
  s.requires_arc = true
  s.authors = {"iScarlett"=>"iScarlett@qq.com"}
  s.homepage = "https://gitlab.com/CoCoKit"
  s.description = "CoCoRNCryptor由自动化打包脚本生成提交,由最新push持续集成,保持最新" 
  s.source = { :git => 'https://gitlab.com/CoCoKit/CoCoRNCryptor-framework.git' }
  s.ios.deployment_target = '8.0'
  s.ios.vendored_framework = 'CoCoRNCryptor.framework'
end
