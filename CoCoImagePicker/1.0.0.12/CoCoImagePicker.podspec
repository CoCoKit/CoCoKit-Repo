Pod::Spec.new do |s|
  s.name = "CoCoImagePicker"
  s.version = "1.0.0.12"
  s.summary = "CoCoActionKit 是CoCo框架下的工具分类"
  s.license = "MIT"
  s.requires_arc = true
  s.authors = {"iScarlett"=>"iScarlett@qq.com"}
  s.homepage = "https://gitlab.com/CoCoKit"
  s.description = "CoCoImagePicker由自动化打包脚本生成提交,由最新push持续集成,保持最新" 
  s.source = { :git => 'https://gitlab.com/CoCoKit/CoCoImagePicker-framework.git' }
  s.ios.deployment_target = '8.0'
  s.ios.vendored_framework = 'CoCoImagePicker.framework'
  s.resource = "Resources/*.bundle"
  s.framework = 'Foundation','UIKit','Photos','AVFoundation','AssetsLibrary'
  s.dependency 'CoCoKit' ,'>= 0'
  s.dependency 'CoCoUIKit' ,'>= 0'
  s.dependency 'CoCoCategorys' ,'>= 0'
  s.dependency 'pop' ,'~> 1.0.9'
  s.dependency 'CoCoReactiveCocoa' ,'>= 0'
end
