Pod::Spec.new do |s|
  s.name = "CoCoImagePicker"
  s.version = "1.0.0.0"
  s.summary = "CoCoActionKit \u{662f}CoCo\u{6846}\u{67b6}\u{4e0b}\u{7684}\u{5de5}\u{5177}\u{5206}\u{7c7b}"
  s.license = "MIT"
  s.authors = {"chenming6"=>"chenming6@staff.weibo.com"}
  s.homepage = "https://iScarlett@bitbucket.org/iScarlett/"
  s.description = "CoCoActionKit \u{662f}CoCo\u{6846}\u{67b6}\u{4e0b}\u{7684}\u{5de5}\u{5177}\u{5206}\u{7c7b}\u{ff0c}\u{5de5}\u{5177}\u{4e00}\u{76f4}\u{5728}\u{66f4}\u{65b0}\n\u{8fd8}\u{5305}\u{62ec}\u{4e00}\u{4e9b}\u{5e38}\u{7528}\u{5b8f}\u{ff0c}\u{4f7f}\u{7528}\u{65f6}\u{6ce8}\u{610f}\u{4e0d}\u{8981}\u{4e0e}\u{5b9a}\u{4e49}\u{7684}\u{5b8f}\u{51b2}\u{7a81},\u{4fee}\u{6539}debug\u{663e}\u{793a}"
  s.frameworks = ["Foundation", "UIKit", "Photos", "AVFoundation","AssetsLibrary"]
  s.requires_arc = true
  s.source = { :git => 'https://gitlab.com/CoCoKit/CoCoImagePicker-framework.git' }
  s.resource = "Resources/CoCoImagePicker.bundle"
  s.ios.deployment_target    = '8.0'
  s.ios.vendored_framework   = 'CoCoImagePicker.framework'
  s.dependency 'CoCoKit' ,'>= 0'
  s.dependency 'CoCoUIKit' ,'>= 0'
  s.dependency 'CoCoCategorys' ,'>= 0'
  s.dependency 'pop' ,'~> 1.0.9'
  s.dependency 'ReactiveCocoa' ,'~> 2.5'
end
